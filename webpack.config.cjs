const path = require("path");

const TerserPlugin = require("terser-webpack-plugin");
const dev = process.env.NODE_ENV === "dev";

let conf = {

    entry : "./browser_min/browser.js", //entry point of the app
    watch:dev,
    mode: 'development',
    output:{
        path:path.resolve("./browser_min"), //out folder
        filename:"dtc.min.js", //out name
    },
    module:{
        rules:[
            {
                test:/\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader'],
				resolve: {
                    fullySpecified: false
                }
            }
        ]
    },

    optimization: {
        minimize: !dev,
        minimizer: [new TerserPlugin()],
    },

}

module.exports = conf;