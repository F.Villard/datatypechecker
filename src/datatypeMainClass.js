import { h } from "..";

const existingDatatypes = {};

/** Properties that are mandatory on each datatype */
var mandatoryProps = [
    {n:"props", t:"array", type:{t:"datatype"} },
    {n:"validate", t:"function"},
    {n:"getDef", t:"function" },
    {n:"setValid", t:"function"},
    {n:"fromBinary", t:"function"},
    {n:"toBinary", t:"function"},
    {n:"getBinarySize", t:"function"},
];

/** Base properties that can be set on any datatype */
var baseProps = [
    {n:"n", t:"string"}, //name
    {n:"t", t:"string"}, //type
    {n:"def", getT:function(dt){return dt.t;} },
    {n:"getT", t:"function", rt:"datatype" },
    {n:"opt",t:"boolean"}, //optional
    {n:"desc",t:"string"}, //description
    {n:"validateDatatype",t:"function"},
    {n:"initDatatype", t:"function"},
    {n:"getInputUI", t:"function"}, //input UI
];

/**
 * Register a new datatype
 * @param {string} name of the datatype (should not already exists)
 * @param {Object} opt Options of the datatypes (must be valid).
 * @returns true if has been registered, false otherwise.
 */
function registerDatatype(name, opt){
    if(existingDatatypes[name]){
        console.warn("This datatype already exists : ", name);
        return false;
    }
    if(validateDatatype(opt)){
        existingDatatypes[name] = new Datatype(opt);
        return true;
    }
    return false;
}


/**
 * Check wether the datatype is valid or not.
 * @param {Object} dt Datatype parameters (must be valid)
 * @param {Boolean} warn If true, log warning infos.
 * @returns 
 */
 function validateDatatype(dt, warn){
    let valid = true, trueDT = true;
    let miss= [];
    mandatoryProps.forEach(e=>{if(dt[e.n] === undefined){
        valid=false;
        trueDT=false;
        miss.push(e);
    }});
    let e = dt;
    let a = Object.assign({}, dt);
    if(!valid){
        if(warn){console.warn("Missing properties:",miss, "to be a valid trueDatatype");}
        e = existingDatatypes[dt.t];
        if(e){e = e.datatype; }
        else if(dt.getT){
            e = existingDatatypes[dt.getT(dt)];
            if(e){e = e.datatype;}
        }
        if(e){
            for(var k in e){ //assign missing props
                if(a[k] == undefined){ a[k] = e[k]; }
            }
        }
    }
    if(e){
        valid = true;
        if(!trueDT && e.validateDatatype){valid = e.validateDatatype(a,warn);}
        //let h = e.props.concat(mandatoryProps,baseProps);
        
        //for(var k in dt){} //should check the properties types
         
    }else if(warn){console.warn("t:",dt.t," isn't a registered datatype");}

    return valid;
}



/**
 * Datatype class
 */
 class Datatype{
    trueType;
    datatype;

    constructor(datatype){
        if(!validateDatatype(datatype, false)){
            throw Error("Invalid datatype : ", datatype,". For more details run 'validateDatatype(yourDatatype, true);'");
        }
        let trueType = true;
        let ex = existingDatatypes[datatype.t];
        mandatoryProps.forEach(e=>{if(datatype[e.n] === undefined){
            trueType=false;
        }});
        if(!trueType){ //copy all parent properties (if not exists)
            for(var k in ex.datatype){
                if(datatype[k] == undefined){ datatype[k] = ex.datatype[k]; }
            }

            if(datatype.initDatatype){
                datatype.initDatatype(datatype);
            }
        }
        
        this.datatype = datatype;
        this.trueType = trueType;
    }

    validate(obj){
        if(this.datatype.opt && obj==undefined){return true;}
        return this.datatype.validate(obj, this.datatype);
    }

    getDefault(){
        let r = this.datatype.def;
        if(this.datatype.opt){return r;}
        if(r == undefined){r = this.datatype.getDef(this.datatype);}
        return this.setValid(r);
    }

    setValid(obj){
        if(this.datatype.opt && obj==undefined){return undefined;}
        return this.datatype.setValid(obj, this.datatype);
    }

    fromBinary(buffer, ctx){
        if(!ctx){ctx = {pos:0, view: new DataView(buffer) };}
        if(this.datatype.opt){
            let isPresent = ctx.view.getUint8(ctx.pos, true);
            ctx.pos += 1;
            if(isPresent == 0){ return undefined;}
        }
        return this.datatype.fromBinary(buffer, ctx, this.datatype);
    }

    toBinary(obj, buffer, ctx){
        if(!buffer){
            let size = this.getBinarySize(obj);
            buffer = new ArrayBuffer(size);
        }
        if(!ctx){
            ctx = {pos:0, view: new DataView(buffer)};
        }
        if(this.datatype.opt){
            ctx.view.setUint8(ctx.pos, obj==undefined?0:1, true);
            ctx.pos += 1;
            if(obj == undefined){return buffer;}
        }
        return this.datatype.toBinary(obj, buffer, ctx, this.datatype);
    }

    getBinarySize(obj, offset){
        let add = 0;
        if(this.datatype.opt){
            if(obj == undefined){return 1;}
            add++;
        }
        return this.datatype.getBinarySize(obj, this.datatype, offset|0) + add;
    }

    getInputUI(obj, userOpt){
        if(typeof userOpt == "function"){
            userOpt = {onchange:userOpt};
        }

        if(this.datatype.getInputUI){
            let elt;
            if(this.datatype.opt){ //optional
                let v = obj;
                if(v == undefined){
                    let r = this.datatype.def;
                    if(r == undefined){r = this.datatype.getDef(this.datatype);}
                    v = this.setValid(r);
                }
                let newUserOpts = Object.assign({},userOpt);
                
                newUserOpts.onchange = function(val){v=val; userOpt.onchange && userOpt.onchange(val);}
                
                let ee = this.datatype.getInputUI(v, this.datatype, newUserOpts);
                let childs = [];
                if(ee instanceof DocumentFragment){
                    for(var i=0;i<ee.children.length;i++){
                        childs.push(ee.children[i]);
                    }
                }
                let setDisabled = (b)=>{
                    if(childs.length){
                        for(var i=0;i<childs.length;i++){
                            childs[i].disabled = b;
                        }
                    }else{
                        ee.disabled = b;
                    }
                };
                let cb = h({elt:"input",type:"checkbox",on:{change:function(){
                    if(this.checked){
                        setDisabled(false);
                        userOpt.oninput && userOpt.oninput(v);
                        userOpt.onchange && userOpt.onchange(v);
                    }else{
                        setDisabled(true);
                        userOpt.oninput && userOpt.oninput(undefined);
                        userOpt.onchange && userOpt.onchange(undefined);
                    }
                }}});
                if(obj==undefined){setDisabled(true); }
                else{cb.checked = true;}
                elt = h({elt:"frag", children:[ee, cb]});
            }else{
                elt = this.datatype.getInputUI(obj, this.datatype, userOpt);
            }
            return elt;
        }else{
            return h({text:"No getInputUI provided"});
        }
    }
}

const _Datatype = Datatype;
export { _Datatype as Datatype };
const _validateDatatype = validateDatatype;
export { _validateDatatype as validateDatatype };
const _registerDatatype = registerDatatype;
export { _registerDatatype as registerDatatype };
