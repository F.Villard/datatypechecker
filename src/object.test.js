
import { Datatype, validateDatatype } from '../index';
import { object } from './object';

test('Validate datatype', () => {
    expect(validateDatatype(object)).toBe(true);
});

test('Validate object correctly', () => {
    let dt = new Datatype({t:"object", properties:[] });
    expect(dt.validate(true)).toBe(false);
    expect(dt.validate({})).toBe(true);
});

test('Validate validateDatatype instanciation', ()=>{
    let dt = {t:"object"};
    expect(validateDatatype(dt)).toBe(false); //missing properties
    dt = {t:"object", properties:[{n:"x", t:"number"}]};
    expect(validateDatatype(dt)).toBe(true);
    //let str = new Datatype(dt);
    //expect(str.validate(false)).toBe(true);
});

test('Validate datatype instanciation', ()=>{
    let dt = {t:"object", properties:[{n:"x", t:"number"}]};
    let str = new Datatype(dt);
    expect(str.validate({})).toBe(false);
    expect(str.validate({x:true})).toBe(false);
    expect(str.validate({x:1})).toBe(true);
    expect(str.validate({x:1,y:2})).toBe(true);
});

test('Buffer works correctly', ()=>{
    let dt = new Datatype({t:"object", properties:[{n:"x", t:"number"},{n:"y", t:"string"}]});
    let a = {x:1.25,y:"toto"};
    let buff = dt.toBinary(a);
    expect(dt.fromBinary(buff)).toEqual(a);
});