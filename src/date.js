import { h } from "./UI/h";


export const dateTime = {
    props:[
        {n:"min", t:"date"},
        {n:"max", t:"date"}
    ],

    validate(obj, opt){
        if(! obj instanceof Date){return false;}
        return true;
    },

    getDef(){return new Date();},
    
    setValid(obj, opt){
        if(! obj instanceof Date){return new Date();}
        return obj;
    },

    fromBinary(bin, ctx, opt){
        let y = ctx.view.getUint16(ctx.pos, true);
        let m = ctx.view.getUint8(ctx.pos+2, true);
        let d = ctx.view.getUint8(ctx.pos+3, true);
        let h = ctx.view.getUint8(ctx.pos+4, true);
        let min = ctx.view.getUint8(ctx.pos+5, true);
        ctx.pos += 6;
        let D = new Date();
        D.setUTCFullYear(y,m,d);
        D.setUTCHours(h,min);
        return D;
    },

    toBinary(obj, bin, ctx, opt){
        ctx.view.setUint16(ctx.pos, obj.getUTCFullYear(), true);
        ctx.view.setUint8(ctx.pos+2, obj.getUTCMonth(), true);
        ctx.view.setUint8(ctx.pos+3, obj.getUTCDate(), true);
        ctx.view.setUint8(ctx.pos+4, obj.getUTCHours(), true);
        ctx.view.setUint8(ctx.pos+5, obj.getUTCMinutes(), true);
        ctx.pos += 6;
        return bin;
    },

    getBinarySize(opt){
        return 6;
    },

    getInputUI(obj, options, userOpt){
        let p = {
            elt:"input",
            type:"datetime-local",
            value:toTZ(obj),
            on:{change:function(v){
                v = fromZ(this.value);
                userOpt.onchange && userOpt.onchange(v);
            }},
        }
        
        if(userOpt.oninput){
            p.on.input = function(){userOpt.oninput(fromZ(this.value));}
        }
        return h(p);
    },

};

function to2(n){
    if(n<10){return "0"+n;}
    return n;
}
function toTZ(date){
    return date.getFullYear()+"-"+to2(date.getMonth()+1)+"-"+to2(date.getDate())+"T"+to2(date.getHours())+":"+to2(date.getMinutes());
}
function fromZ(date){
    return new Date(date);
}
