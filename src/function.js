import { getInput } from "./UI/inputBase";


export const fx = {
    props:[
        {n:"rt", t:"datatype"}, //return type
        {n:"args", t:"array", type:{t:"object",properties:[
            {n:"name", t:"string"},
            {n:"type", t:"datatype"},
        ]}} //arguments
    ],

    validate(obj, opt){
        if(typeof obj !== "function"){return false;}
        return true;
    },

    getDef(){return function(x){return x;}},
    
    setValid(obj, opt){
        if(typeof obj !== "function"){
            console.warn("Could not set a valid function that do what you should expect. You may encounter bugs from now.", obj)
            return ()=>{};
        }
        return obj;
    },

    fromBinary(bin, ctx, opt){
        let size = ctx.view.getUint32(ctx.pos, true);
        ctx.pos += 4;
        let a = new Uint8Array(bin,ctx.pos, size);
        var enc = new TextDecoder("utf-8");
        return new Function("return " + enc.decode(a) )();
    },

    toBinary(obj, bin, ctx, opt){
        let enc = new TextEncoder().encode(obj.toString() );
        ctx.view.setUint32(ctx.pos, enc.byteLength, true);
        ctx.pos += 4;
        new Uint8Array(bin,ctx.pos, enc.byteLength).set(enc);
        ctx.pos += enc.byteLength;
        return bin;
    },

    getBinarySize(obj, opt){
        let enc = new TextEncoder().encode(obj.toString() );
        return 4+enc.byteLength;
    },

    getInputUI:getInput,

};
