
//import {Datatype} from "../index";
//import {number} from "./number";

import { Datatype, validateDatatype } from '../index';
import { number } from './number';

test('Validate datatype', () => {
    expect(validateDatatype(number)).toBe(true);
});

test('Validate numbers correctly', () => {
    let num = new Datatype(number);
    expect(num.validate(0)).toBe(true);
    expect(num.validate("0")).toBe(false);
});

test('Validate instanciation', ()=>{
    let dt = {t:"number"};
    expect(validateDatatype(dt)).toBe(true);
});

test('Instanciation works correctly', ()=>{
    let dt = {t:"number", min:5};
    let num = new Datatype(dt);
    expect(num.validate(0)).toBe(false);
    expect(num.validate(5)).toBe(true);
    expect(num.getDefault()).toBe(5);
});

test('Buffer works correctly', ()=>{
    let dt = new Datatype({t:"number"});
    let buff = dt.toBinary(8.23);
    expect(dt.fromBinary(buff)).toBe(8.23);
});