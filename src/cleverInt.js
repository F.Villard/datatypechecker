import { getInput } from "./UI/inputBase";

const maxInt32 = (1<<30)*2;

const number = {
    props:[
        {n:"min", t:"number", opt:true},
        {n:"max", t:"number", opt:true},
    ],

    validate(obj, opt){
        if(typeof obj !== "number"){return false;}
        if(opt.setValid(obj, opt) !== obj){return false;}
        return true;
    },

    getDef(){return 0;},
    
    setValid(obj, opt){
        let r = opt.def == undefined ? opt.getDef(opt) : opt.def;
        switch(typeof obj){
            case "number": r = parseInt(obj); break; //can't |0 since it can be bigger than int32
            case "string": let a = parseInt(obj); if(!isNaN(a)){r = a;} break;
            default:break;
        }
        if(opt.min !==undefined && opt.min>r){r = opt.min;}
        if(opt.max !==undefined && opt.max<r){r = opt.max;}
        return r;
    },

    fromBinary(bin, ctx, opt){
        let r = ctx.view.getUint8(ctx.pos, true);
        ctx.pos += 1;
        if(r<128){
            return r;
        }else{
            switch(r){
                default:
                    return r;
                case 129:
                    r = ctx.view.getUint8(ctx.pos, true);
                    ctx.pos += 1;
                    return r;
                case 130:
                    r = ctx.view.getUint16(ctx.pos, true);
                    ctx.pos += 2;
                    return r;
                case 131:
                    r = ctx.view.getUint32(ctx.pos, true);
                    ctx.pos += 4;
                    return r;
                case 132:
                    let m = ctx.view.getUint32(ctx.pos, true);
                    r = ctx.view.getUint32(ctx.pos+4, true);
                    ctx.pos += 8;
                    return m*maxInt32+r;
                case 133:
                    r = ctx.view.getInt8(ctx.pos, true);
                    ctx.pos += 1;
                    return r;
                case 134:
                    r = ctx.view.getInt16(ctx.pos, true);
                    ctx.pos += 2;
                    return r;
                case 135:
                    r = ctx.view.getInt32(ctx.pos, true);
                    ctx.pos += 4;
                    return r;
            }
        }
        
    },

    toBinary(obj, bin, ctx, opt){
        if(obj>=0){
            if(obj<255){
                if(obj<128){
                    ctx.view.setUint8(ctx.pos, obj, true);
                    ctx.pos += 1;
                }else{
                    ctx.view.setUint8(ctx.pos, 129, true);
                    ctx.view.setUint8(ctx.pos+1, obj, true);
                    ctx.pos += 2;
                }
            }else{
                if(obj<65536){
                    ctx.view.setUint8(ctx.pos, 130, true);
                    ctx.view.setUint16(ctx.pos+1, obj, true);
                    ctx.pos += 3;
                }else{
                    if(obj<maxInt32){
                        ctx.view.setUint8(ctx.pos, 131, true);
                        ctx.view.setUint32(ctx.pos+1, obj, true);
                        ctx.pos += 5;
                    }else{
                        ctx.view.setUint8(ctx.pos, 132, true);
                        let m = Math.floor(obj/maxInt32);
                        let r = obj%maxInt32;
                        ctx.view.setUint32(ctx.pos+1, m, true);
                        ctx.view.setUint32(ctx.pos+5, r, true);
                        ctx.pos += 9;
                    }
                }
            }
        }else{
            if(obj>-128){
                ctx.view.setUint8(ctx.pos, 133, true);
                ctx.view.setInt8(ctx.pos+1, obj, true);
                ctx.pos += 2;
            }else if(obj>-65536){
                ctx.view.setUint8(ctx.pos, 133, true);
                ctx.view.setInt16(ctx.pos+1, obj, true);
                ctx.pos += 3;
            }else{
                ctx.view.setUint8(ctx.pos, 133, true);
                ctx.view.setInt32(ctx.pos+1, obj, true);
                ctx.pos += 5;
            }
        }
        return bin;
    },

    getBinarySize(obj, opt){
        if(obj>=0){
            if(obj<255){
                if(obj<128){
                    return 1;
                }else{
                    return 2;
                }
            }else{
                if(obj<65536){
                    return 3;
                }else{
                    if(obj<maxInt32){
                        return 5;
                    }else{
                        return 9;
                    }
                }
            }
        }else{
            if(obj>-128){
                return 2;
            }else if(obj>-65536){
                return 3;
            }else{
                return 5;
            }
        }
    },

    getInputUI:getInput,

};

export const cleverInt = number;
