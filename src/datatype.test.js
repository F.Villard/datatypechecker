
import { Datatype, validateDatatype } from '../index';
import { datatype } from './datatype';

test('Validate datatype', () => {
    expect(validateDatatype(datatype)).toBe(true);
});

test('Validate datatype correctly', () => {
    let dt = new Datatype(datatype);
    expect(dt.validate(0)).toBe(false);
    expect(dt.validate({t:"number"})).toBe(true);
});

test('Validate instanciation', ()=>{
    let dt = {t:"datatype"};
    expect(validateDatatype(dt)).toBe(true);
    let str = new Datatype(dt);
    expect(str.validate(false)).toBe(false);
    expect(str.validate({t:"string"})).toBe(true);
});