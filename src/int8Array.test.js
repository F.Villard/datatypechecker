import { Datatype, validateDatatype } from '../index';
import { int8Array } from './int8Array';

test('Validate datatype', () => {
    expect(validateDatatype(int8Array)).toBe(true);
});

test('Validate int8Array correctly', () => {
    let num = new Datatype(int8Array);
    expect(num.validate([1,2,3])).toBe(false);
    expect(num.validate(new Int8Array())).toBe(true);
});

test('Validate instanciation', ()=>{
    let dt = {t:"int8Array"};
    expect(validateDatatype(dt)).toBe(true);
});

test('Instanciation works correctly', ()=>{
    let dt = {t:"int8Array"};
    let num = new Datatype(dt);
    expect(num.validate([])).toBe(false);    
    expect(num.validate(new Int8Array() )).toBe(true);
});

test('Buffer works correctly', ()=>{
    let dt = new Datatype({t:"int8Array"});
    let a = new Int8Array([0,1,2,3,4]);
    let buff = dt.toBinary(a);
    expect(dt.fromBinary(buff)).toEqual(a);
});