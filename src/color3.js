import { h } from "./UI/h";


function validateChar(c){
    let s = c.charCodeAt(0);
    return ( s>47 && s<58 ) || ( s>64 && s<71 ) || ( s>96 && s<103 );
}
function toString2(x){
    let r = x.toString(16);
    if(x<16){return "0"+r;}
    return r;
}

export const color3 = {
    props:[
    ],

    validate(obj, opt){
        if(typeof obj !== "string"){return false;}
        let valid = true;
        switch(obj.length){
            default:
                return false;
            case 4:
                valid = obj[0]=="#" && validateChar(obj[1])&& validateChar(obj[2])&& validateChar(obj[3]);
                break;
            case 7:
                valid = obj[0]=="#" && validateChar(obj[1])&& validateChar(obj[2])&& validateChar(obj[3])&& validateChar(obj[4])&& validateChar(obj[5])&& validateChar(obj[6]);
                break;
        }
        return valid;
    },

    getDef(){return "#000";},
    
    setValid(obj, opt){
        let r = opt.def == undefined ? opt.getDef(opt) : opt.def;
        if(typeof obj !== "string"){return r;}
        let i = 0, valid;
        switch(obj.length){
            default:
                return r;
            case 4:
                if(obj[0]!=="#"){return r;}
                i = 1;
            case 3:
                valid = validateChar(obj[i])&& validateChar(obj[i+1])&& validateChar(obj[i+2]);
                if(!valid){return r;}
                r = "#"+obj[i]+obj[i]+obj[i+1]+obj[i+1]+obj[i+2]+obj[i+2];
                break;
            case 7:
                if(obj[0]!=="#"){return r;}
                i = 1;
            case 6:
                valid = validateChar(obj[i])&& validateChar(obj[i+1])&& validateChar(obj[i+2])&& validateChar(obj[i+3])&& validateChar(obj[i+4])&& validateChar(obj[i+5]);
                if(!valid){return r;}
                r = "#"+obj[i]+obj[i+1]+obj[i+2]+obj[i+3]+obj[i+4]+obj[i+5];
                break;
        }
        return r;
    },

    fromBinary(bin, ctx, opt){
        let r = ctx.view.getUint8(ctx.pos, true);
        let g = ctx.view.getUint8(ctx.pos+1, true);
        let b = ctx.view.getUint8(ctx.pos+2, true);
        console.log(r,g,b);
        ctx.pos += 3;
        return "#"+toString2(r)+toString2(g)+toString2(b);
    },

    toBinary(obj, bin, ctx, opt){
        let r,g,b;
        switch(obj.length){
            case 4:
                r = parseInt(obj[1], 16); r = r*16+r;
                g = parseInt(obj[2], 16); g = g*16+g;
                b = parseInt(obj[3], 16); b = b*16+b;
                break;
            case 7:
                r = parseInt(obj.slice(1,3), 16);
                g = parseInt(obj.slice(3,5), 16);
                b = parseInt(obj.slice(5,7), 16);
                break;
            default:
                break;            
        }
        console.log(r,g,b);
        ctx.view.setUint8(ctx.pos, r, true);
        ctx.view.setUint8(ctx.pos+1, g, true);
        ctx.view.setUint8(ctx.pos+2, b, true);
        ctx.pos += 3;
        return bin;
    },

    getBinarySize(opt){
        return 3;
    },

    getInputUI(obj, options, userOpt){
        let p = {
            elt:"input",
            type:"color",
            value:options.setValid(obj, options),
            on:{change:function(v){
                v = this.value;
                userOpt.onchange && userOpt.onchange(v);
            }},
        }
        if(userOpt.oninput){
            p.on.input = function(){userOpt.oninput(this.value);}
        }
        return h(p);
    },

};
