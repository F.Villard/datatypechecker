import { Datatype, validateDatatype } from './datatypeMainClass';

const array = {
    props:[
        {n:"length", t:"number", opt:true}
    ],
    

    validate(obj, opt){
        if(!(obj instanceof Int16Array)){return false;}
        if(opt.length && opt.length !== obj.length){return false;}
        return true;
    },

    getDef(opt){return new Int16Array(opt.length || 0);},
    
    setValid(obj, opt){
        let r = obj;
        if(!(obj instanceof Int16Array)){
            if(Array.isArray(obj)){
                r = new Int16Array(obj);
            }else{
                r = new Int16Array(opt.length || 0);
            }
        }
        if(opt.length && opt.length !== r.length){ //set the right amount
            if(opt.length>r.length){
                r= new Int16Array(opt.length).set(r);
            }else{
                r = r.slice(0,opt.length);
            }
        }
        return r;
    },

    fromBinary(bin, ctx, opt) {
        let size = 0;
        if (opt.length) { size = opt.length; }
        else {
            size = ctx.view.getUint32(ctx.pos, true);
            ctx.pos += 4;
        }
        let offset = (ctx.pos) % 2;
        if (offset !== 0) { ctx.pos += 2 - offset; }
        let r = new Int16Array(bin, ctx.pos, size);
        ctx.pos += size * 2;
        return r;
    },

    toBinary(obj, bin, ctx, opt) {
        if (!opt.length) {
            ctx.view.setUint32(ctx.pos, obj.length, true);
            ctx.pos += 4;
        }
        let offset = (ctx.pos) % 2;
        if (offset !== 0) { ctx.pos += 2 - offset; }
        new Int16Array(bin, ctx.pos, obj.length).set(obj);
        ctx.pos += obj.length * 2;
        return bin;
    },

    getBinarySize(obj, opt, cs){
        let size = opt.length?0:4;
        let offset =  (cs+size)%2;
        if(offset!==0){size += 2 - offset;}
        size += obj.length * 2;
        return size;
    }

};

export const int16Array = array;
