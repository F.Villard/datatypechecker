import { Datatype, validateDatatype } from './datatypeMainClass';

const datatype = {
    props:[
    ],

    validate(obj, opt){
        if(obj instanceof Datatype){return true;}
        return validateDatatype(obj);
    },

    getDef(){return new Datatype({t:"number"});},
    
    setValid(obj, opt){
        if(obj instanceof Datatype){return obj;}
        if(!validateDatatype(obj)){
            if(typeof obj == "string"){
                if(validateDatatype({t:obj})){return new Datatype({t:obj});}
            }
            console.warn("Could not automatically set a valid datatype. You may find bugs");
            return this.getDef();
        }
        return new Datatype(obj);
    },

    fromBinary(bin, ctx, opt){

    },

    toBinary(bin, ctx, opt){

    },

    getBinarySize(opt){
        return 8;
    }

};

const _datatype = datatype;
export { _datatype as datatype };
