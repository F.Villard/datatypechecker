import { h } from "./UI/h";


export const enume = {
    props:[
        {n:"values", t:"array"}
    ],

    validate(obj, opt){
        if(typeof obj !== "number" && (obj|0) !== obj && obj<0 && obj>=opt.values.length){return false;}
        return true;
    },

    getDef(){return 0;},
    
    setValid(obj, opt){
        if(typeof obj !== "number" && (obj|0) !== obj && obj<0 && obj>=opt.values.length){return 0;}
        return obj;
    },

    fromBinary(bin, ctx, opt){
        let r = ctx.view.getUint8(ctx.pos, true);
        ctx.pos += 1;
        return r;
    },

    toBinary(obj, bin, ctx, opt){
        ctx.view.setUint8(ctx.pos, obj, true);
        ctx.pos += 1;
        return bin;
    },

    getBinarySize(opt){
        return 1;
    },

    getInputUI(obj, options, userOpt){
        let p = {
            elt:"select",
            on:{change:function(v){
                v = this.value|0;
                userOpt.onchange && userOpt.onchange(v);
            }},
        }
        let child = [];
        for(var i=0,n=options.values.length;i<n;i++){
            let o = {elt:"option", value:i, HTML:options.values[i]};
            if(obj == i){o.selected="true";}
            child.push(o)
        }
        p.child = child;
        if(userOpt.oninput){
            p.on.input = function(){userOpt.oninput(this.value|0);}
        }
        return h(p);
    },

};
