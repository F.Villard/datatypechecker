
//import {Datatype} from "../index";
//import {number} from "./number";

import { Datatype, validateDatatype } from '../index';
import { string } from './string';

test('Validate datatype', () => {
    expect(validateDatatype(string)).toBe(true);
});

test('Validate strings correctly', () => {
    let dt = new Datatype(string);
    expect(dt.validate(0)).toBe(false);
    expect(dt.validate("0")).toBe(true);
});

test('Validate instanciation', ()=>{
    let dt = {t:"string"};
    expect(validateDatatype(dt)).toBe(true);
    let str = new Datatype(dt);
    expect(str.validate("hello")).toBe(true);
});

test('Buffer works correctly', ()=>{
    let dt = new Datatype({t:"string"});
    let buff = dt.toBinary("Hello World !");
    expect(dt.fromBinary(buff)).toBe("Hello World !");
});