import { h } from "./UI/h";


const boolean = {
    props:[
    ],

    validate(obj, opt){
        if(typeof obj !== "boolean"){return false;}
        return true;
    },

    getDef(){return false;},
    
    setValid(obj, opt){
        let r = !!obj;
        return r;
    },

    fromBinary(bin, ctx, opt){
        let r = ctx.view.getUint8(ctx.pos, true);
        ctx.pos += 1;
        return !!r;
    },

    toBinary(obj, bin, ctx, opt){
        ctx.view.setUint8(ctx.pos, obj?1:0, true);
        ctx.pos += 1;
        return bin;
    },

    getBinarySize(opt){
        return 1;
    },

    getInputUI(obj, options, userOpt){
        let p = {
            elt:"input",
            type:"checkbox",
            on:{change:function(v){
                v = this.checked;
                userOpt.onchange && userOpt.onchange(v);
            }},
        }
        if(obj){p.checked=true}
        if(userOpt.oninput){
            p.on.input = function(){userOpt.oninput(this.checked);}
        }
        return h(p);
    },

};

const _boolean = boolean;
export { _boolean as boolean };
