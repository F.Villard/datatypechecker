import { getInput } from "./UI/inputBase";


const number = {
    props:[
        {n:"min", t:"number", opt:true},
        {n:"max", t:"number", opt:true},
    ],

    validate(obj, opt){
        if(typeof obj !== "number"){return false;}
        if(opt.setValid(obj, opt) !== obj){return false;}
        return true;
    },

    getDef(){return 0;},
    
    setValid(obj, opt){
        let r = opt.def == undefined ? opt.getDef(opt) : opt.def;
        switch(typeof obj){
            case "number": r = obj|0; break;
            case "string": let a = parseInt(obj); if(!isNaN(a)){r = a;} break;
            default:break;
        }
        if(opt.min !==undefined && opt.min>r){r = opt.min;}
        if(opt.max !==undefined && opt.max<r){r = opt.max;}
        if(r>65535){r = 65535;}
        if(r<0){r = 0;}
        return r;
    },

    fromBinary(bin, ctx, opt){
        let r = ctx.view.getUint16(ctx.pos, true);
        ctx.pos += 2;
        return r;
    },

    toBinary(obj, bin, ctx, opt){
        ctx.view.setUint16(ctx.pos, obj, true);
        ctx.pos += 2;
        return bin;
    },

    getBinarySize(obj, opt){
        return 2;
    },

    getInputUI:getInput,

};

export const uInt16 = number;
