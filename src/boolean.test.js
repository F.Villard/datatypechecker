
import { Datatype, validateDatatype } from '../index';
import { boolean } from './boolean';

test('Validate datatype', () => {
    expect(validateDatatype(boolean)).toBe(true);
});

test('Validate boolean correctly', () => {
    let dt = new Datatype(boolean);
    expect(dt.validate(0)).toBe(false);
    expect(dt.validate(true)).toBe(true);
});

test('Validate instanciation', ()=>{
    let dt = {t:"boolean"};
    expect(validateDatatype(dt)).toBe(true);
    let str = new Datatype(dt);
    expect(str.validate(false)).toBe(true);
});

test('Buffer works correctly', ()=>{
    let dt = new Datatype({t:"boolean"});
    let buff = dt.toBinary(true);
    expect(dt.fromBinary(buff)).toBe(true);
});