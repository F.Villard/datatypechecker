import { getInput } from "./UI/inputBase";


const string = {
    props:[
    ],

    validate(obj, opt){
        if(typeof obj !== "string"){return false;}
        let enc = new TextEncoder().encode(obj);
        if(enc.byteLength>255){return false;}
        return true;
    },

    getDef(){return "";},
    
    setValid(obj, opt){
        let r = obj;
        if(typeof obj !== "string"){
            if(obj.toString){
                r = obj.toString();
            }
            else{r = "";}
        }
        let enc = new TextEncoder().encode(r);
        if(enc.byteLength>255){
            r = new TextDecoder("utf-8").decode(enc.slice(0,255));
        }
        return r;
    },

    fromBinary(bin, ctx, opt){
        let size = ctx.view.getUint8(ctx.pos, true);
        ctx.pos += 1;
        let a = new Uint8Array(bin,ctx.pos, size);
        var enc = new TextDecoder("utf-8");
        ctx.pos += a.byteLength;
        return enc.decode(a);
    },

    toBinary(obj, bin, ctx, opt){
        let enc = new TextEncoder().encode(obj);
        ctx.view.setUint8(ctx.pos, enc.byteLength, true);
        ctx.pos += 1;
        new Uint8Array(bin,ctx.pos, enc.byteLength).set(enc);
        ctx.pos += enc.byteLength;
        return bin;
    },

    getBinarySize(obj, opt){
        let enc = new TextEncoder().encode(obj);
        return 1+enc.byteLength;
    },

    getInputUI:getInput,

};

export const shortString = string
