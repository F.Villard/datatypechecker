import { Datatype, validateDatatype } from './datatypeMainClass';

const array = {
    props:[
        {n:"length", t:"number", opt:true}
    ],
    

    validate(obj, opt){
        if(!(obj instanceof Uint8Array)){return false;}
        if(opt.length && opt.length !== obj.length){return false;}
        return true;
    },

    getDef(opt){return new Uint8Array(opt.length || 0);},
    
    setValid(obj, opt){
        let r = obj;
        if(!(obj instanceof Uint8Array)){
            if(Array.isArray(obj)){
                r = new Uint8Array(obj);
            }else{
                r = new Uint8Array(opt.length || 0);
            }
        }
        if(opt.length && opt.length !== r.length){ //set the right amount
            if(opt.length>r.length){
                r= new Uint8Array(opt.length).set(r);
            }else{
                r = r.slice(0,opt.length);
            }
        }
        return r;
    },

    fromBinary(bin, ctx, opt){
        let size = 0;
        if(opt.length){size = opt.length;}
        else{
            size = ctx.view.getUint32(ctx.pos, true);
            ctx.pos += 4;
        }
        return new Uint8Array(bin,ctx.pos, size); // Math.ceil(ctx.pos/2)*2
    },

    toBinary(obj, bin, ctx, opt){
        if(!opt.length){
            ctx.view.setUint32(ctx.pos, obj.length, true);
            ctx.pos += 4;
        }
        new Uint8Array(bin,ctx.pos, obj.length).set(obj);  // Math.ceil(ctx.pos/2)*2
        return bin;
    },

    getBinarySize(obj, opt){
        let size = opt.length?0:4;
        size += obj.length; // * 1;
        return size;
    }

};

export const uint8Array = array;
