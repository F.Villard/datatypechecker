

var registeredElement = {};

var translations = {}; //dico

function registerElement(type, fx){
    registeredElement[type] = fx;
}

function buildElement(props, options){    
    if(typeof props != "object"){props = {};}
    
    //create the element
    var elt = props.elt || "div";
    switch(typeof elt){
        case "string":
            if(registeredElement[elt]){
                elt = registeredElement[elt](props, options || props.opt);
            }else{
                switch(elt){
                    default:
                        if(props.NS){
                            elt = document.createElementNS(props.NS, elt);
                        }else{
                            elt = document.createElement(elt);
                        }
                        break;
                    case "frag":
                        elt = document.createDocumentFragment();
                        break;
                }
            }
            break;
        case "function":
            elt = elt(props, options);
            break;
        case "object":
            break;
        default:
            break;
    }

    //apply properties
    for(var prop in props){
        var v = props[prop];
        switch(prop){
            default:
                let ind = ["value"].indexOf(prop);
                if(ind<0){
                    elt.setAttribute(prop,v);
                }else{
                    elt[prop] = v;
                }
                break;
            case "NS":
            case "opt":
            case "elt":
                break;
            case "getRef":
                v(elt);
                break;
			case "class":
                var array = v.split(' ');
                for (var i = 0, n = array.length; i < n; i++) {
                    elt.classList.add(array[i]);
                }
                break;
			case "HTML":elt.innerHTML = v;break;
			case "Text":elt.innerText = v;break;
			case "child": //append multiple elements
				for(var i=0;i<v.length;i++){
					elt.appendChild(buildElement(v[i]));
				}
				break;
            case "children": //append multiple elements
				for(var i=0;i<v.length;i++){
					elt.appendChild(v[i]);
				}
				break;
            case "setAttrNS":
                for(var k in v){elt.setAttributeNS(null, k,v[k]);}
				break;
			case "setAttr":
				for(var k in v){elt.setAttribute(k,v[k]);}
				break;
			case "setProps":
				for(var k in v){elt[k]=v[k];}
				break;
			case "style":
				for(var k in v){elt.style[k]=v[k];}
				break;
            case "on": //add listener
                for(var k in v){
                    switch(k){
                        case "down":
                            elt.addEventListener("mousedown", v[k], true);
                            elt.addEventListener("touchstart", v[k], true);
                            break;
                        case "up":
                                elt.addEventListener("mouseup", v[k], true);
                                elt.addEventListener("touchend", v[k], true);
                                break;
                        case "pick":
                            elt.addEventListener("click",v[k],true);
                            break;
                        case "drag":
                            addDragEvent(elt, v[k]);
                            break;
                        default:
                            elt.addEventListener(k,v[k],true);
                            break;
                    }
                }
                break;
        }
    }

    return elt;
}


function addDragEvent(elt, opt){
    if(typeof opt != "object"){
        opt = {};
    }
    elt.addEventListener("mousedown", function(e){
        let button = e.button;
        let move = function(e){
            opt.drag && opt.drag(e);
        };
        let end = function(e){
            if(e.button != button){return;}
            window.removeEventListener("mousemove", move, true);
            window.removeEventListener("mouseup", end, true);
            opt.endDrag && opt.endDrag(e);
            opt.end && opt.end(e);
        };

        window.addEventListener("mousemove", move, true);
        window.addEventListener("mouseup", end, true);
        opt.startDrag && opt.startDrag(e);
        opt.start && opt.start(e);
    }, true);

    elt.addEventListener("touchstart", function(e){
        let button = e.changedTouches[0];
        let move = function(e){
            if(e.changedTouches.indexOf(button)<0){return;}
            opt.drag && opt.drag(e);
        };
        let end = function(e){
            if(e.changedTouches.indexOf(button)<0){return;}
            window.removeEventListener("touchmove", move, true);
            window.removeEventListener("touchend", end, true);
            opt.endDrag && opt.endDrag(e);
        };

        window.addEventListener("touchmove", move, true);
        window.addEventListener("touchend", end, true);
        opt.startDrag && opt.startDrag(e);
    }, true);
}


buildElement.setTranslations = function(o){
    for(var k in o){
        translations[k] = o[k];
    }
}

buildElement.getTranslation = buildElement.getT = function(t){
    let k = translations[t]
    if(k){return k;}
    return t;
}

buildElement.translatePage = function(){
    var l = document.body.querySelectorAll("[trad]");
    for(var i=0,n=l.length;i<n;i++){
        l[i].innerText = this.getT(l[i].getAttribute("trad"));
    }
    var l = document.body.querySelectorAll("[tradVal]");
    for(var i=0,n=l.length;i<n;i++){
        l[i].value = this.getT(l[i].getAttribute("tradVal"));
    }
    var l = document.body.querySelectorAll("[tradHover]");
    for(var i=0,n=l.length;i<n;i++){
        let T = this.getT(l[i].getAttribute("tradHover"));
        l[i].setAttribute("hoverTitle", T );
    }
}

buildElement.addHover = function(args, opt){
    if(typeof opt != "object"){opt = {};}
    let options = {
        position: opt.position || function(elt, evt){
            //let ww = window.innerWidth;
            let wh = window.innerHeight;
            let bb = elt.getBoundingClientRect();
            let M = 0, W = 0, H=0;
            if(bb.top>100){ //bottom
                H = wh - bb.top; 
            }else{ //top
                M |= 1;
                H = bb.bottom;
            }
            W = bb.left + bb.width/2;
            M |= 2;
            return [W,H,M];
        },
        marginV: opt.marginV || 0,
        marginH: opt.marginH || 0,
        timeout: opt.timeout==undefined?500:opt.timeout,
        hide : opt.hide || function(elt){elt.style.display = "none"},
        show : opt.show || function(elt){elt.style.display = "block"}

    };
    let d = h(args);
    options.hide(d);
    d.style.position = "fixed";
    d.style.pointerEvents = "none";
    let target = null;
    let timeout = 0;
    window.addEventListener("mousemove", function(e){
        let elt = e.target;

        if(elt != target){
            target = elt;
            timeout && clearTimeout(timeout);
            options.hide(d);
            timeout = this.setTimeout(function(){
                let k = target.getAttribute("hoverTitle");
                if(k){
                    options.show(d);
                    d.innerText = k;
                    let [W,H,M] = options.position(target, e);
                    if(M&1){
                        d.style.top = H+options.marginV+"px";
                        d.style.bottom = "";
                    }else{
                        d.style.bottom = H+options.marginV+"px";
                        d.style.top = "";
                    }
                    if(M&2){
                        d.style.left = W+options.marginH+"px";
                        d.style.right = "";
                    }else{
                        d.style.right = W+options.marginH+"px";
                        d.style.left = "";
                    }
                    
                }
            }, options.timeout);
        }

    }, true);
    document.body.appendChild(d);
}


buildElement.watch = function(obj, val, change){
    let v = obj[val];
    Object.defineProperty(obj, val, {
        get:()=>{return v;},
        set:(va)=>{
            v = va;
            change(v);
        }
    });
}


export const h = buildElement;
const _registerElement = registerElement;
export { _registerElement as registerElement };

