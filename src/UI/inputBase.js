import { h } from "./h"


export const getInput = (obj, options, userOpt)=>{
    
    let p = {
        elt:"input",
        type:"text",
        on:{change:function(){
            let v = options.setValid(this.value,options);
            this.value = v;
            userOpt.onchange && userOpt.onchange(v);
        }},
        value:obj
    }

    let isRange = options.hasOwnProperty("min") && options.hasOwnProperty("max") && options.hasOwnProperty("step");
    if(isRange){
        let r  = h({ elt:"input", type:"range",min:options.min, max:options.max, step:options.step, style:{margin:"-0.3em 0 0 0"}});
        let d =  h({ elt:"input", type:"text"});
        let br = h({elt:"br"});
        
        r.value = d.value = obj;
        r.onchange = d.onchange = function(){
            let v = options.setValid(this.value,options);
            r.value = d.value = v;
            userOpt.onchange && userOpt.onchange(v);
        }
        r.oninput = function(){
            let v = options.setValid(this.value,options);
            r.value = d.value = v;
            userOpt.oninput && userOpt.oninput(v);
        }
        d.oninput = function(){
            let v = options.setValid(this.value,options);
            r.value = v;
            userOpt.oninput && userOpt.oninput(v);
        }

        return h({elt:"frag", children:[d,br,r]});
    }
    if(userOpt.oninput){
        p.on.input = function(){
            let v = options.setValid(this.value,options);
            userOpt.oninput(v);
        }
    }
    return h(p);
}

