
import { Datatype, validateDatatype } from '../index';
import { fx } from './function';

test('Validate datatype', () => {
    expect(validateDatatype(fx)).toBe(true);
});

test('Validate function correctly', () => {
    let dt = new Datatype(fx);
    expect(dt.validate(0)).toBe(false);
    expect(dt.validate(function(){})).toBe(true);
});

test('Validate instanciation', ()=>{
    let dt = {t:"function"};
    expect(validateDatatype(dt)).toBe(true);
    let str = new Datatype(dt);
    expect(str.validate(function(){})).toBe(true);
});

test('Buffer works correctly', ()=>{
    let dt = new Datatype({t:"function"});
    let a = function(x){return x*2;};
    let buff = dt.toBinary(a);
    let fx2 = dt.fromBinary(buff);
    expect(fx2(0)).toEqual(0);
    expect(fx2(2)).toEqual(4);
});