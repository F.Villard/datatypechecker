import { Datatype, validateDatatype } from './datatypeMainClass';
import { h } from './UI/h';


const object = {
    props:[
        {n:"properties", t:"array", type:{t:"datatype"}}
    ],

    validateDatatype(dt, warn){
        if(!dt.properties){
            if(warn){console.warn("Missing properties in ", dt);}
            return false;
        }
        for(var i=0;i<dt.properties.length;i++){
            let dtt = dt.properties[i];
            if(dtt instanceof Datatype){dtt = dtt.datatype}
            else{
                if(!validateDatatype(dtt)){ if(warn){ console.warn("This is not a valid datatype",dtt);} return false; }
            }
            if(!dtt.n){
                if(warn){console.warn("Missing property name 'n' in ", dtt);}
                return false;
            }
        }
        return true;
    },

    initDatatype(dt){
        for(var i=0;i<dt.properties.length;i++){
            if(!(dt.properties[i] instanceof Datatype)){
                dt.properties[i] = new Datatype(dt.properties[i]);
            }
        }
    },

    validate(obj, opt){
        if(typeof obj !== "object"){return false;}
        for(var i=0;i<opt.properties.length;i++){
            let dt = opt.properties[i];
            if(!dt.validate(obj[dt.datatype.n])){return false;}
        }
        return true;
    },

    getDef(opt){
        let r = {};
        for(var i=0;i<opt.properties.length;i++){
            let dt = opt.properties[i];
            r[dt.datatype.n] = dt.getDefault();
        }
        return r;
    },
    
    setValid(obj, opt){
        let r = typeof obj == "object"?obj:{};
        for(var i=0;i<opt.properties.length;i++){
            let dt = opt.properties[i];
            r[dt.datatype.n] = dt.setValid(r[dt.datatype.n]);
        }
        return r;
    },

    fromBinary(bin, ctx, opt){
        let r = {};
        for(var i=0;i<opt.properties.length;i++){
            let dt = opt.properties[i]; //.datatype;
            let dtt = dt.datatype;
            r[dtt.n] = dt.fromBinary(bin,ctx);
        }
        return r;
    },

    toBinary(obj, bin, ctx, opt){
        for(var i=0;i<opt.properties.length;i++){
            let dt = opt.properties[i]; //.datatype;
            let dtt = dt.datatype;
            dt.toBinary(obj[dtt.n],bin,ctx);
        }
        return bin;
    },

    getBinarySize(obj, opt, cs){
        let size = 0;
        for(var i=0;i<opt.properties.length;i++){
            let dt = opt.properties[i]; //.datatype;
            let dtt = dt.datatype;
            size += dt.getBinarySize(obj[dtt.n], cs+size);
        }
        return size;
    },

    getInputUI(obj, options, userOpt){
        let p = {
            elt:"table",
            class:"inputObject",
            child:[]
        }
        let goc = (atr)=>{
            let O = Object.assign({}, userOpt);
            if(userOpt.onchange){
                O.onchange = function(v){
                    obj[atr] = v;
                    userOpt.onchange(obj);
                    checkCond();
                }
            }else{
                O.onchange = function(v){
                    obj[atr] = v;
                    checkCond();
                }
            }
            if(userOpt.oninput){
                O.oninput = function(v){
                    obj[atr] = v;
                    userOpt.oninput(obj);
                }
            }else{
                O.oninput = function(v){
                    obj[atr] = v;
                }
            }
            return O;
        }
        let checkCond = ()=>{
            for(var i=0, n=options.properties.length; i<n;i++){
                let dt = options.properties[i].datatype;
                let inds = indexes[i];
                if(dt.cond && !checkCondition(obj, dt.cond)){
                    for(var j=0,m=inds.length;j<m;j++){
                        table.children[inds[j]].style.display = "none";
                    }
                }else{
                    for(var j=0,m=inds.length;j<m;j++){
                        table.children[inds[j]].style.display = "table-row";
                    }
                }
            }
        };
        let indexes = [];
        let ind = 0;
        for(var i=0, n=options.properties.length; i<n;i++){
            let dt = options.properties[i];
            let a = dt.datatype.n;
            let td = {elt:"td", Text:a};
            if(userOpt.translate){
                td.Text = h.getT(a);
                td.trad = a;
            }
            if(Array.isArray(dt.datatype.properties)){
                td.colspan = 2;
                p.child.push(
                    {elt:"tr",child:[td]},
                    {elt:"tr",child:[{elt:"td", colspan:2, style:{paddingLeft:"1em"},children:[dt.getInputUI(obj[a], goc(a))]}]}
                );
                indexes.push([ind, ind+1]);
                ind+=2;
            }else{
                p.child.push({elt:"tr",child:[td,{elt:"td", children:[dt.getInputUI(obj[a], goc(a))]}]});
                indexes.push([ind++]);
            }
            
            //
        }
        let table = h(p);
        checkCond();
        return table;
    },

};

function checkCondition(obj, cond){
    if(cond[0]=="&&"){
        let r = checkCondition(obj, cond[1]);
        for(var i=2;i<cond.length;i++){
            r = r && checkCondition(obj, cond[i]); 
        }
        return r; 
    }else if(cond[0]=="||"){
        let r = checkCondition(obj, cond[1]);
        for(var i=2;i<cond.length;i++){
            r = r || checkCondition(obj, cond[i]); 
        }
        return r;
    }else{
        let v = obj[cond[0]];
        let val = cond[2];
        switch(cond[1]){
            case "=":
            case "==":
                return v == val;
            case "===":
                return v === val;
            case "!=":
                return v != val;
            case "!==":
                return v !== val;
            case "<":
                return v < val;
            case ">":
                return v > val;
            case "<=":
                return v <= val;
            case ">=":
                return v >= val;
            default:
                console.warn("Unknown operator", cond[1]);
                return false;
        }
    }
}


const _object = object;
export { _object as object };
