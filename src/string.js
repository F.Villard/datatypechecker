import { getInput } from "./UI/inputBase";


export const string = {
    props:[
    ],

    validate(obj, opt){
        if(typeof obj !== "string"){return false;}
        return true;
    },

    getDef(){return "";},
    
    setValid(obj, opt){
        if(typeof obj !== "string"){
            if(obj.toString){
                return obj.toString();
            }
            return "";
        }
        return obj;
    },

    fromBinary(bin, ctx, opt){
        let size = ctx.view.getUint32(ctx.pos, true);
        ctx.pos += 4;
        let a = new Uint8Array(bin,ctx.pos, size);
        var enc = new TextDecoder("utf-8");
        ctx.pos += a.byteLength;
        return enc.decode(a);
    },

    toBinary(obj, bin, ctx, opt){
        let enc = new TextEncoder().encode(obj);
        ctx.view.setUint32(ctx.pos, enc.byteLength, true);
        ctx.pos += 4;
        new Uint8Array(bin,ctx.pos, enc.byteLength).set(enc);
        ctx.pos += enc.byteLength;
        return bin;
    },

    getBinarySize(obj, opt){
        let enc = new TextEncoder().encode(obj);
        return 4+enc.byteLength;
    },

    getInputUI:getInput,
};
