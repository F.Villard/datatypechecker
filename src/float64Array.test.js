import { Datatype, validateDatatype } from '../index';
import { float64Array } from './float64Array';

test('Validate datatype', () => {
    expect(validateDatatype(float64Array)).toBe(true);
});

test('Validate float64Array correctly', () => {
    let num = new Datatype(float64Array);
    expect(num.validate([1,2,3])).toBe(false);
    expect(num.validate(new Float64Array())).toBe(true);
});

test('Validate instanciation', ()=>{
    let dt = {t:"float64Array"};
    expect(validateDatatype(dt)).toBe(true);
});

test('Instanciation works correctly', ()=>{
    let dt = {t:"float64Array"};
    let num = new Datatype(dt);
    expect(num.validate([])).toBe(false);
    expect(num.validate(new Float64Array() )).toBe(true);
});

test('Buffer works correctly', ()=>{
    let dt = new Datatype({t:"float64Array"});
    let a = new Float64Array([0,1,2,3,4.24]);
    let buff = dt.toBinary(a);
    expect(dt.fromBinary(buff)).toEqual(a);
});

test('float64Array works correctly with length', ()=>{
    let dt = new Datatype({t:"float64Array", length:5});
    expect(dt.validate(new Float64Array([0,1,2,3]) )).toBe(false);
    let a = new Float64Array([0,1,2,3,4.24]);
    let buff = dt.toBinary(a);
    expect(dt.fromBinary(buff)).toEqual(a);
});