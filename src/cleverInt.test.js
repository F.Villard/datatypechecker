
import { Datatype, validateDatatype } from '../index';
import { cleverInt } from './cleverInt';


test('Validate datatype', () => {
    expect(validateDatatype(cleverInt)).toBe(true);
});

test('Validate instanciation', ()=>{
    let dt = {t:"cleverInt"};
    expect(validateDatatype(dt)).toBe(true);
    let str = new Datatype(dt);
    expect(str.validate(15)).toBe(true);
    expect(str.validate(15.5)).toBe(false);
});

test('Validate binary', ()=>{
    let dt = {t:"array", type:{t:"cleverInt"}};
    let cdt = new Datatype(dt);
    let a = [0,15,-20,458,66666,(1<<30)*4];
    let buff = cdt.toBinary(a);
    let b = cdt.fromBinary(buff);
    expect(b).toEqual(a);
});