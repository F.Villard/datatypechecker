import { Datatype, validateDatatype } from './datatypeMainClass';

const array = {
    props:[
        {n:"type", t:"datatype"},
        {n:"length", t:"number", opt:true}
    ],
    
    //type:{t:"number"},

    validateDatatype(dt, warn){
        if(!dt.type){
            if(warn){console.warn("Missing type in ", dt);}
            return false;
        }
        if(!dt.type instanceof Datatype){
            if(!validateDatatype(dt.type, warn)){
                if(warn){console.warn("Invalid datatype",dt.type);}
                return false;
            }
        }
        return true;
    },
    
    initDatatype(dt){
        if(!(dt.type instanceof Datatype)){
            dt.type = new Datatype(dt.type);
        }
    },

    validate(obj, opt){
        if(!Array.isArray(obj)){return false;}
        let dt = opt.type;
        for(var i=0;i<obj.length;i++){
            if(!dt.validate(obj[i])){return false;}
        }
        if(opt.length && opt.length !== obj.length){return false;}
        return true;
    },

    getDef(){return [];},
    
    setValid(obj, opt){
        let r = Array.isArray(obj)?obj:[];
        let dt = opt.type;
        for(var i=0;i<r.length;i++){
            r[i] = dt.setValid(r[i]);
        }
        if(opt.length && opt.length !== r.length){ //set the right amount
            if(opt.length>r.length){
                for(var i=r.length;i<=opt.length;i++){
                    r.push(dt.getDefault());
                }
            }else{
                r = r.slice(0,opt.length);
            }
        }
        return r;
    },

    fromBinary(bin, ctx, opt){
        let r = [];
        let dt = opt.type;
        let size = 0;
        if(opt.length){size = opt.length;}
        else{
            size = ctx.view.getUint32(ctx.pos, true);
            ctx.pos += 4;
        }
        for(var i=0;i<size;i++){
            r.push(dt.fromBinary(bin,ctx));
        }
        return r;
    },

    toBinary(obj, bin, ctx, opt){
        if(!opt.length){
            ctx.view.setUint32(ctx.pos, obj.length, true);
            ctx.pos += 4;
        }
        let dt = opt.type;
        for(var i=0;i<obj.length;i++){
            dt.toBinary(obj[i],bin,ctx);
        }
        return bin;
    },

    getBinarySize(obj, opt, cs){
        let size = opt.length?0:4;
        let dt = opt.type;
        for(var i=0;i<obj.length;i++){
            size += dt.getBinarySize(obj[i], cs+size);
        }
        return size;
    }

};

const _array = array;
export { _array as array };
