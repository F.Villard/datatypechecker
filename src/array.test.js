
import { Datatype, validateDatatype } from '../index';
import { array } from './array';

test('Validate datatype', () => {
    expect(validateDatatype(array, true)).toBe(true);
});

test('Validate array correctly', () => {
    let dt = new Datatype(array);
    expect(dt.validate({})).toBe(false);
    expect(dt.validate([])).toBe(true);
});

test('Validate instanciation', ()=>{
    let dt = {t:"array"};
    expect(validateDatatype(dt)).toBe(false);
    dt = {t:"array", type:{t:"string"} };
    expect(validateDatatype(dt)).toBe(true);
    let str = new Datatype(dt);
    expect(str.validate(["ok","super"])).toBe(true);
    expect(str.validate(["ok","super",1])).toBe(false);
});

test('Buffer works correctly', ()=>{
    let dt = new Datatype({t:"array", type:{n:"x", t:"number"} });
    let a = [0.25,0.32,1.45,-25];
    let buff = dt.toBinary(a);
    expect(dt.fromBinary(buff)).toEqual(a);
});