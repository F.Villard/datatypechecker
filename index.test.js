
let {Datatype, validateDatatype, registerDatatype} = require("./index");


test("No redefine number", ()=>{
    expect(registerDatatype("number",{t:"number"})).toBe(false);
});

test("Can define newnumber", ()=>{
    expect(registerDatatype("newnumber",{t:"number", def:5})).toBe(true);
    let dt = new Datatype({"t":"newnumber"});
    expect(dt.getDefault()).toBe(5);
});

test("Complex datatype buffer", ()=>{
    let x = {t:"object", properties:[
        {n:"name", t:"shortString"},
        {n:"buffer", t:"int32Array"},
        {n:"array", t:"array", type:{t:"float32"}},
    ]}
    expect(validateDatatype(x)).toBe(true);
    let dt = new Datatype(x); 
    let a = {name:"test", buffer:new Int32Array([0,1,2,3]), array:[4,5,6] };
    expect(dt.validate(a)).toBe(true);
    let buff = dt.toBinary(a);
    let b = dt.fromBinary(buff);
    expect(b).toEqual(a);
})