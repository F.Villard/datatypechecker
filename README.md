# datatypechecker

## Description
This is a simple realtime datatype validator, UI generator, and binary file manipulator.

## Installation
Install with npm :
```
npm install datatypechecker
```

## Usage
A basic example of data type validation would be :
```js
import {Datatype} from 'datatypechecker';
let dt = new Datatype({t:"string"});
dt.validate("3"); // -> returns true
dt.validate(3); // -> returns false
```

Test if your datatype is valid by using :
```js
import {validateDatatype} from 'datatypechecker';
validateDatatype({t:"string"}); // -> returns true
```

Register new complex dataypes by using :
```js
import {registerDatatype} from 'datatypechecker';
registerDatatype("name", {
    props:[], // array of datatype optional properties of the datatype.
    validate(obj, opt){ return b;}, // tells if an obj is valid or not base on its options.
    getDef(){return 0;}, // returns the default value of this datatype    
    setValid(obj, opt){return r;}, // set an obj valid according to its datatype options.
    fromBinary(bin, ctx, opt){return r;}, // read the value from an arraybuffer.
    toBinary(obj, bin, ctx, opt){return bin;},// write the value in an arraybuffer.
    getBinarySize(obj, opt){return r;} // get the size in octet that will be taken by obj.
});
```

All base js types are already pre-registered. This includes :
- number
- string
- shortString : a string with less than 256 characters
- boolean
- object : has the property "properties" wich is an array of datatypes with the property "n". See example below.
- function : any JS function or arrow function.
- array : an array of the same type of data. Has the property "type" that is a datatype describing the content of the array. See example below.
- uInt8
- uInt16
- uInt32
- int8
- int16
- int32
- float32
- float64
- int8Array
- uint8Array
- int16Array
- uint16Array
- int32Array
- uint32Array
- float32Array
- float64Array

You can also register any valid composed datatype as your own datatype in order to reuse it in more complex datatypes.
```js
import {registerDatatype, Datatype} from 'datatypechecker';

registerDatatype("myCustomObject", { t:"object", properties:[
	{n:"name", t:"shortString"},
	{n:"buffer", t:"int32Array"},
	{n:"array", t:"array", type:{t:"float32"}},
]});

//you can now make : 

let dt = new Datatype({t:"myCustomObject"});

// or use it in complex objects : 

let dt = new Datatype({t:"object", properties:[
	{n:"name", t:"string"},
	{n:"data", t:"myCustomObject"},
]});

```

Read and write JS to compact binary files like this : 
```js
import {Datatype} from 'datatypechecker';

let dt = new Datatype({ t:"object", properties:[
	{n:"name", t:"shortString"},
	{n:"buffer", t:"int32Array"},
	{n:"array", t:"array", type:{t:"float32"}},
]});

let x = {name:"test", buffer:new Int32Array([0,1,2,3]), array:[4,5,6] };
if(dt.validate(x)){ //if x is a valid implementation of the dt Datatype (wich should be the case)
	
	let buffer = dt.toBinary(x); //generate an arraybuffer containing x.
	
    let y = dt.fromBinary(buffer); // y is now a JS object that has been generated from the buffer and that should be strictly equivalent to x.
	
}
```


## License
This project is free to use. Feel free to do anything you want with it.
