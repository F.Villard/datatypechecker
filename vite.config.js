import { defineConfig } from 'vite'
import { resolve } from 'path'

const dev = process.env.NODE_ENV === "dev";


let conf = {
  /*resolve: {
    // conditions: ["main"], // Doesn't work -- option 1
    // mainFields: ["main"], // Doesn't work -- option 2
    alias: [                 // This worked  -- option 4
       {
         find: "axios-cache-adapter",
         replacement: resolve(__dirname, "/node_modules/axios-cache-adapter/dist/cache.node.js"),
       },
    ],
	environment: 'node'
  },*/
  server:{
	  port:3003,
	  host:"localhost",
	  watch: {
		ignored: ["**/node_modules/**"],
	  },
  },
  build: {
	
	//target:"es2015",
	minify:true,
	emptyOutDir: true,
	//ssr: true,
	lib: {
      // Could also be a dictionary or array of multiple entry points
      entry: resolve(__dirname, 'index.js'),
      name: 'datatypechecker',
      // the proper extensions will be added
      fileName: 'datatypechecker'
    },
  }
}

if(dev){
	
	conf.build.rollupOptions =  {
      input: {
        main: resolve(__dirname, 'index.html'),
      }
    }
	
}

export default defineConfig(conf)