const express = require('express');
const cors = require('cors');


const app = express();
const port = 3003;


app.use(cors({
    origin: "*"
}));


app.use(express.static('./'));

app.listen(port, () => {
  console.log(`Serving on port ${port}!`)
});