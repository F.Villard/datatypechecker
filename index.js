
import {Datatype,validateDatatype, registerDatatype} from './src/datatypeMainClass';
import {number} from "./src/number";
import {string} from "./src/string";
import {shortString} from "./src/shortString";
import {boolean} from "./src/boolean";
import {object} from "./src/object";
import {fx} from "./src/function";
import {array} from "./src/array";
import {datatype} from "./src/datatype";
import {uInt8} from "./src/uInt8";
import {uInt16} from "./src/uInt16";
import {uInt32} from "./src/uInt32";
import {int8} from "./src/int8";
import {int16} from "./src/int16";
import {int32} from "./src/int32";
import {float32} from "./src/float32";
import {float64} from "./src/float64";
import {int8Array} from "./src/int8Array";
import {uint8Array} from "./src/uint8Array";
import {int16Array} from "./src/int16Array";
import {uint16Array} from "./src/uint16Array";
import {int32Array} from "./src/int32Array";
import {uint32Array} from "./src/uint32Array";
import {float32Array} from "./src/float32Array";
import {float64Array} from "./src/float64Array";
import {enume} from "./src/enum";
import {color3} from "./src/color3";
import {dateTime} from "./src/date";
import { cleverInt } from './src/cleverInt';

import {h, registerElement} from "./src/UI/h";



registerDatatype("number", number);
registerDatatype("string", string);
registerDatatype("shortString", shortString);
registerDatatype("boolean", boolean);
registerDatatype("object", object);
registerDatatype("function", fx);
registerDatatype("array", array);
registerDatatype("datatype", datatype);
registerDatatype("uInt8", uInt8);
registerDatatype("uInt16", uInt16);
registerDatatype("uInt32", uInt32);
registerDatatype("int8", int8);
registerDatatype("int16", int16);
registerDatatype("int32", int32);
registerDatatype("float32", float32);
registerDatatype("float64", float64);
registerDatatype("int8Array", int8Array);
registerDatatype("uint8Array", uint8Array);
registerDatatype("int16Array", int16Array);
registerDatatype("uint16Array", uint16Array);
registerDatatype("int32Array", int32Array);
registerDatatype("uint32Array", uint32Array);
registerDatatype("float32Array", float32Array);
registerDatatype("float64Array", float64Array);
registerDatatype("enum", enume);
registerDatatype("color3", color3);
registerDatatype("dateTime", dateTime);
registerDatatype("cleverInt", cleverInt);


const _Datatype = Datatype;
export { _Datatype as Datatype };
const _validateDatatype = validateDatatype;
export { _validateDatatype as validateDatatype };
const _registerDatatype = registerDatatype;
export { _registerDatatype as registerDatatype };
const _h = h;
export { _h as h };
const _registerElement = registerElement;
export { _registerElement as registerElement };